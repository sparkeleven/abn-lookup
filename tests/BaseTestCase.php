<?php

namespace Tests;

/**
 * Base TestCase class.
 *
 * @author Theodore Yaosin <theo@sparkeleven.com.au>
 */
abstract class BaseTestCase extends \Orchestra\Testbench\TestCase
{
}
