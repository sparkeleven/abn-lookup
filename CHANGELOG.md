# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2020-12-07

Very first version

### Added

- Search methods
    * `searchByAbn` (uses SearchByABNv202001)
    * `searchByAsic` (uses SearchByASICv201408)
    * `searchByName` (uses ABRSearchByName)
- Laravel support
    * [Service provider](https://bitbucket.org/sparkeleven/abn-lookup/src/1.0.0/src/AbnLookupServiceProvider.php)
    * [Dedicated config file](https://bitbucket.org/sparkeleven/abn-lookup/src/1.0.0/config/abn-lookup.php)



[1.0.0]: https://bitbucket.org/sparkeleven/abn-lookup/src/1.0.0
