<?php

namespace SparkEleven\AbnLookup;

use Illuminate\Support\ServiceProvider;
use SparkEleven\AbnLookup\Facades\AbnLookup as AbnLookupFacade;
use SparkEleven\AbnLookup\Services\AbnLookupService;

/**
 * ABN Lookup Service Provider.
 *
 * @author Theodore Yaosin <theo@sparkeleven.com.au>
 * @package SparkEleven\AbnLookup
 */
class AbnLookupServiceProvider extends ServiceProvider
{
    /**
     * Path to translation files inside this package.
     *
     * @var string
     */
    const PATH_TRANSLATIONS = __DIR__.'/../resources/lang';

    /**
     * Path to view files inside this package.
     *
     * @var string
     */
    const PATH_VIEWS = __DIR__.'/../resources/views';

    /**
     * Path to config file inside this package.
     *
     * @var string
     */
    const PATH_CONFIG = __DIR__.'/../config/abn-lookup.php';

    /**
     * All of the container singletons that should be registered.
     *
     * @var array
     */
    public $singletons = [
        AbnLookupFacade::class => AbnLookupService::class,
    ];


    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(self::PATH_TRANSLATIONS, 'abn-lookup');
        $this->loadViewsFrom(self::PATH_VIEWS, 'abn-lookup');

        $this->publishes([
            self::PATH_CONFIG => config_path('abn-lookup.php'),
        ], 'abn-lookup:config');

        $this->publishes([
            self::PATH_TRANSLATIONS => resource_path('lang/vendor/abn-lookup'),
        ], 'abn-lookup:translations');

        $this->publishes([
            self::PATH_VIEWS => resource_path('views/vendor/abn-lookup'),
        ], 'abn-lookup:views');

        // Initiate service
        AbnLookupService::reset(
            config('abn-lookup.auth_guid'),
            config('abn-lookup.wsdl'),
            config('abn-lookup.wsdl_cache'),
        );
    }
}
